/*

Simple paddle to HID converter by Dawid SQ6EMM (2023)

Works with Arduino Micro Pro 
Require HID-Project libraries ( https://github.com/NicoHood/HID/wiki/RawHID-API )

*/

#include "HID-Project.h"

const int pinLeft = 2;
const int pinRight = 3;

uint8_t rawhidData[8];
unsigned int paddleState = 0;
unsigned int lastPaddleChange;
unsigned int lastPaddleState;
unsigned int PaddleDebounceDelay = 2;
byte hidData[2] = { 255, 0 };

void setup() {
  pinMode(pinLeft, INPUT_PULLUP);
  pinMode(pinRight, INPUT_PULLUP);
}

void loop() {
  bitWrite(paddleState, 1, !digitalRead(pinLeft));
  bitWrite(paddleState, 2, !digitalRead(pinRight));
  hidData[1] = paddleState;
  RawHID.write(hidData, sizeof(hidData));
  delay(1);
}
