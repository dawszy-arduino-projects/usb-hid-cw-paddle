#!/usr/bin/env python3

'''
Iambic logic based on: http://m0xpd.blogspot.com/2012/12/keyer-on-rpi.html (M0XPD)
Tone generation based on: https://stackoverflow.com/questions/8299303/generating-sine-wave-sound-in-python

'''

import array
import math
import time
import pyaudio
import hid
import time
import socket

UDP_IP="192.168.253.3"
UDP_PORT=31337

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

wpm = 18
DitLength = 60/(50*wpm) # PARIS method, as reference 12WPM dit takes 100 ms
DahLength = DitLength*3

p = pyaudio.PyAudio()
fs = 44100

stream = p.open(format=pyaudio.paFloat32,
                channels=1,
                rate=fs,
                frames_per_buffer=128,
                output=True)

volume = 0.4
f = 600.0  # tone frequency in Hertz

num_samples = int(fs * DitLength)
samples = [volume * math.sin(2 * math.pi * k * f / fs) for k in range(0, num_samples)]
dit_bytes = array.array('f', samples).tobytes()

num_samples = int(fs * DahLength)
samples = [volume * math.sin(2 * math.pi * k * f / fs) for k in range(0, num_samples)]
dah_bytes = array.array('f', samples).tobytes()

def playTone(elem):
  global dit_bytes
  global dah_bytes
  if elem == "dit": stream.write(dit_bytes)
  if elem == "dah": stream.write(dah_bytes)

vid=0x2341
pid=0x8036
length=8

h = hid.device()
h.open(vid, pid)

def read_from_hid(dev, timeout):
  try:
    data = dev.read(length, timeout)
  except IOError as e:
    print ('Error reading response: {}'.format(e))
    return None
  return data

last = "none"

def SendDit(DitLength):
 """ Sends a Dit..."""
 KeyLength=DitLength
 KeyDown("dit",KeyLength)
 Last_Element="dit"
 return Last_Element

def SendDah(DahLength):
 """ Sends a Dah..."""
 KeyDown("dah",DahLength)
 Last_Element="dah"
 return Last_Element

def SendSpace(DitLength):
 """ Wait for inter-element space..."""
 time.sleep(DitLength)
 return

def KeyDown(elem,KeyLength):
 """ Keys the TX """

 # set the output to Key Down...
 sock.sendto(b"1",(UDP_IP,UDP_PORT))
 playTone(elem);
 sock.sendto(b"0",(UDP_IP,UDP_PORT))
 return

def paddleState():
  key="none"
  rbuffer = read_from_hid(h,3)
  if rbuffer and rbuffer[0] == 255:
    val = rbuffer[1]
    if val == 4: key="right"
    if val == 2: key="left"
    if val == 6: key="both"
    if val == 0: key="none"
  return(key)

last_paddle = "none"
while True:

 paddle=paddleState()

 if (paddle == "left"):
  last_paddle = "left"
  while (paddle == "left"):
   last=SendDit(DitLength)
   SendSpace(DitLength)
   paddle=paddleState()

 elif (paddle == "right"):
  last_paddle = "right"
  while (paddle == "right"):
   last=SendDah(DahLength)
   SendSpace(DitLength)
   paddle=paddleState()

 elif (paddle == "both"):
  last_paddle = "both"
  while (paddle == "both"):
   if last == "dit":
    last=SendDah(DahLength)
   elif last == "dah":
    last=SendDit(DitLength)
   SendSpace(DitLength)
   paddle=paddleState()

 elif (paddle == "none" and last_paddle == "both"):
  if last == "dit":
   last=SendDah(DahLength)
  elif last == "dah":
   last=SendDit(DitLength)
  last="none"
  last_paddle="none"

 time.sleep(DitLength/4)

stream.stop_stream()
stream.close()
p.terminate()
