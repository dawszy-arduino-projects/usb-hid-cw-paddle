# Intro

This is very simple code that you can use to:

* connect your CW paddles to arduino Pro Micro and by that to your computer (Linux tested only)
* decode iambic keying into standard straight keying using provided Python code and send this straight keying over the network to your ICOM radio that supports either USB or COM DTR keying (additionaly you will hear locally generated sidetone)
* receive straight keying over UDP and key your ICOM radio with Python code (Linux tested only)

Keyer device can by Arduino Pro Micro (default) or eCoderPlus.

# Devices with working code

On Linux please do not forget to update your udev with following rules:

## For Adruino Pro Micro:
```
SUBSYSTEM=="usb", ATTRS{idVendor}=="2341", ATTRS{idProduct}=="8036", MODE="0664", GROUP="plugdev"
```

## For EXPERT-ELECT eCoderPlus (SunSDR):
```
SUBSYSTEM=="usb", ATTRS{idVendor}=="1fc9", ATTRS{idProduct}=="0003", MODE="0664", GROUP="plugdev"
```
