#!/usr/bin/env python3

'''
Simplest possible UDP CW receiver - no buffer currently
Keying ICOM radio with capability to KEY over USB DTR
'''

import socket
import serial

UDP_IP="0"
UDP_PORT=31337

icomUSB = serial.Serial('/dev/ttyACM1', 115200, timeout=None)
icomUSB.setDTR(False);

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.settimeout(1)
sock.bind((UDP_IP,UDP_PORT))

while True:
  try:
    data, addr = sock.recvfrom(1)
    if int(data) == 1:
      icomUSB.setDTR(True);
      icomUSB.flush();
    elif int(data) == 0:
      icomUSB.setDTR(False);
      icomUSB.flush();
  except KeyboardInterrupt:
    icomUSB.setDTR(False);
    icomUSB.flush();
    exit(0)
  except:
    icomUSB.setDTR(False);
    icomUSB.flush();
