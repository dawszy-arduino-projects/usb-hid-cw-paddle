/*

Simple paddle to HID converter by Dawid SQ6EMM (2023)

Works with Arduino Micro Pro 
Require HID-Project libraries ( https://github.com/NicoHood/HID/wiki/RawHID-API )

To be used with: https://morsecode.world/international/keyer.html

*/

#include "HID-Project.h"

const int pinLeft = 2;
const int pinRight = 3;

unsigned int lastPinLeftState;
unsigned int lastPinRightState;
unsigned int PaddleDebounceDelay = 2;
byte hidData[2] = { 255, 0 };

void setup() {
  pinMode(pinLeft, INPUT_PULLUP);
  pinMode(pinRight, INPUT_PULLUP);
  Keyboard.begin();
}

void loop() {
  if (!digitalRead(pinLeft) && lastPinLeftState == 0) {
    Keyboard.press('z');
    lastPinLeftState = 1;
  }
  if (digitalRead(pinLeft) && lastPinLeftState == 1) {
    Keyboard.release('z');
    lastPinLeftState = 0;
  }
  if (!digitalRead(pinRight) && lastPinRightState == 0) {
    Keyboard.press('x');
    lastPinRightState = 1;
  }
  if (digitalRead(pinRight) && lastPinRightState == 1) {
    Keyboard.release('x');
    lastPinRightState = 0;
  }  
  delay(2);
}
